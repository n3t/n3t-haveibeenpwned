Settings
========

Available settings for this plugin in plugin manager are:

#### Check during login

If enabled, check is done during login process. If password is compromised, 
warning will be displayed to user.

By default this is **Enabled**.

#### Check on user save

If enabled, check is done during user profile save (frontend even backend). 
If password is compromised, warning will be displayed to user, or save will 
be disabled (based on 'Disable save on compromise' settings bellow).

By default this is **Enabled**.

#### Disable save on compromise

If enabled, and password is found as compromised during profile save, 
user is not allow to choose such password, and has to enter new one.

By default this is **Disabled**.

#### Max. compromise tolerance

Max. number of occurrences in HaveIBeenPwned.com database. 
If password is known as compromised, there is also info, how many times it 
has been seen in public databases. You can limit here minimum number of 
occurrences to consider password as compromised.

By default this is set to **0** - no tolerance.