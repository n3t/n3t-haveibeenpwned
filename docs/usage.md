Usage
=====

There is no special usage of this plugin. After it is installed and enabled, 
it starts to work automatically. Whenever user log in, register or change his 
password (based on settings), the password is checked against [HaveIBeenPwned.com][HIBP]
database, and if it is found as compromised, user is notified by warning.

You can also choose to block such passwords. In such case, whenever user changes his password, 
and the new password is compromised, he is forced to change it.

Note, that this plugin is using standard [Joomla! CMS][JOOMLA] events. If you use
some 3rd party component to manage users, it could, but doesn't need to work, based
on way, how the component is written.

[HIBP]: https://haveibeenpwned.com/
[JOOMLA]: https://www.joomla.org/
