Credits
=======

Thanks to these projects and contributors:

- [HaveIBeenPwned.com][HIBP] service
- [Joomla! CMS][Joomla]
- [Bongovo!][BONGOVO] for Czech translation
- [Czech Joomla community][JOOMLAPORTAL] for testing, support and comments on development
- hdcms for French translation

[HIBP]: https://haveibeenpwned.com/
[JOOMLA]: https://www.joomla.org
[BONGOVO]: https://www.bongovo.cz/
[JOOMLAPORTAL]: https://www.joomlaportal.cz/
