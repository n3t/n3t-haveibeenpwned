Release notes
=============

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

[4.0.x]
-------

### [4.0.1] - 2021-08-28

#### Changed
- improved Joomla 4 compatibility

### [4.0.0] - 2021-07-17

- Initial public release