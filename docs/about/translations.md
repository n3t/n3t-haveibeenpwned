Translations
============

List of known translations by community: 

- __[![cs-cz](../images/flags/cz.png) Czech translation][CS_CZ]__ by [Bongovo!][BONGOVO]
- __[![fr-fr](../images/flags/fr.png) French translation][FR_FR]__ by hdcms

!!! info "Translations"
    Translations listed here are not official part of n3t HaveIBeenPwned plugin.
    It is created and maintained by volunteers. For any info about translation, bug reports
    etc. contact directly their authors.

[CS_CZ]: https://www.bongovo.cz/ke-stazeni/category/227-user-n3t-haveibeenpwned
[FR_FR]: https://bitbucket.org/n3t/n3t-haveibeenpwned/downloads/fr-FR.plg_user_hibp.zip

[BONGOVO]: https://www.bongovo.cz/

