n3t HaveIBeenPwned
==================

![Version](https://img.shields.io/badge/Latest%20version-4.0.1-informational)
![Release date](https://img.shields.io/badge/Release%20date-2022--08--28-informational)

n3t HaveIBeenPwned is [Joomla! CMS][JOOMLA] plugin integrating [HaveIBeenPwned.com][HIBP].

HaveIBeenPwned.com is project gatehring informations about password breaches, and collecting compromised passwords.
It means, if your password (not necessarily password of your account) was somewhere published by hackers, who hacked some
passwords database, it will be listed there. And if so, the password is propably **not secure** anymore.

n3t HaveIBeenPwned checks users passwords during login process, and during registration, or whenever user changes his password.
If compromised password is detected, warning is displayed to user or, optionally, user cannot use such password.

Note thaht communication with [HaveIBeenPwned.com][HIBP] API is absolutely anonymous, no passwords are sent 
through the API, only small parts of hash of password is being used (for further info see
[API documentation][API]).

Installation
------------

Install n3t HaveIBeenPwned as any other [Joomla! CMS][JOOMLA] plugin. After installation do not
forget to publish it, and optionally check it's options to fine-tune it's behavior.

[HIBP]: https://haveibeenpwned.com/
[API]: https://haveibeenpwned.com/API/v3#SearchingPwnedPasswordsByRange
[JOOMLA]: https://www.joomla.org/
