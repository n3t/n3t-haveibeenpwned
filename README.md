n3t HaveIBeenPwned
==================

![Version](https://img.shields.io/badge/Latest%20version-4.0.1-informational)
![Release date](https://img.shields.io/badge/Release%20date-2022--08--28-informational)

[![Joomla! CMS](https://img.shields.io/badge/Joomla!%20CMS-V3.10-green)][JOOMLA]
[![Joomla! CMS](https://img.shields.io/badge/Joomla!%20CMS-V4.x-green)][JOOMLA]
[![PHP](https://img.shields.io/badge/PHP-V7.2-green)][PHP]
[![Documentation Status](https://readthedocs.org/projects/n3t-haveibeenpwned/badge/?version=latest)](https://n3t-haveibeenpwned.readthedocs.io/en/latest/?badge=latest)

[HaveIBeenPwned.com][HIBP] implementation for Joomla! CMS.

Install instructions
----------------------------
 * install package using Joomla! installer
 * enable plugin
 * [read the documentation][DOCS]

[JOOMLA]: https://www.joomla.org
[PHP]: https://www.php.net
[HIBP]: https://haveibeenpwned.com/
[DOCS]: https://n3t-haveibeenpwned.readthedocs.io/
